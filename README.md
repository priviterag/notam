#Decoding Notam#
##Background##
Here at Stratajet we regularly deal with unusual problems. The private jet industry is still slightly backwards in terms of technology and there are countless areas where standardization and communication could be improved.

An example of this is NOTAMs (Notice to Airmen), which is a notice filed with an aviation authority to alert pilots of potential hazards along a flight route or at a location that could affect the safety of a flight. 

NOTAMs have some level of standardization, but the information contained within them can vary in presentation and format. An example of a NOTAM containing hours of operation is below. The various sections are explained below the example (see http://en.wikipedia.org/wiki/NOTAM for more details).

**Example NOTAM**

```
B0519/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/5746N01404E005
A) ESGJ
B) 1502271138
C) 1503012359
E) AERODROME HOURS OF OPS/SERVICE MON­WED 0500­1830 THU 0500­2130 FRI
0730­2100 SAT 0630­0730, 1900­2100 SUN CLOSE
```

### Title Section ###
The first line contains NOTAM identification (series, sequence number, and year of issue), the type of operation (NEW, REPLACE, or CANCEL), as well as a reference to a previously­ issued NOTAM (for NOTAMR and NOTAMC only).

### Q Section ###
The "Q" line holds information about who the NOTAM affects along with a basic NOTAM description. This line can be encoded/decoded from tables defined by ICAO. This allows NOTAMs to be displayed electronically.

### A Section ###
The "A" line is the ICAO code of the affected aerodrome or FIR for the NOTAM. The area of influence of the NOTAM can be several hundreds of kilometres away from the originating aerodrome.

### B Section ###
The "B" line contains the start date and time

### C Section ###
The "C" line contains the finish date and time of the NOTAM. The date is in the format YY/MM/DD and the times are given in Universal Coordinated Time; also known as GMT or Zulu time.

### D Section ###
Sometimes a "D" line may be present. This gives a miscellaneous diurnal time for the NOTAM if the hours of effect are less than 24 hours a day, e.g. parachute dropping exercises tend to occur for short periods of a few hours during the day, but may be repeated over many days.

### E Section ###
The "E" line is the full NOTAM description. It is in English but heavily abbreviated. These abbreviations can be encoded/decoded by tables defined by ICAO.

### F Section ###
When present, "F" and "G" lines detail the height restrictions of the NOTAM. Typically SFC means surface height or ground level and UNL is unlimited height. Other heights are given in feet or flight level or a combination of the two.

### Exercise ###
This exercise requires you to build a simple rails application that reads a series of NOTAMs, parses them and displays the parsed data in a readable and standardised way. For the purposes of this exercise, we are only interested in NOTAMs containing information for AERODROME HOURS OF OPS/SERVICE, all other NOTAMs can be ignored.

The exercise should be completed by creating a simple Rails application with an input page and a results page. The input page should allow a user to paste a series of NOTAMs (the notam_data.txt) and submit them to the server. 

The server will then parse the NOTAMs and display the parsed results in tabular form. 

For each NOTAM record of interest, a row of data with eight columns should be produced ­ the first should contain the ICAO code (e.g. ESGJ, ESGT) of the NOTAM, the remaining seven should indicate opening hours for each day of the week starting with Monday. For example, the following NOTAM:

```
B0508/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/5746N01404E005
A) ESGJ B) 1503020000 C) 1503222359
E) AERODROME HOURS OF OPS/SERVICE MON 0500­2000 TUE­THU 0500­2100 FRI
0545­2100 SAT0630­0730 1900­2100 SUN CLOSED
CREATED: 26 Feb 2015 10:54:00
SOURCE: EUECYIYN
```

should produce the following output:
```
ESGJ | 0500­2000 | 0500­2100 | 0500­2100 | 0500­2100 | 0545­2100 | 0630­0730 1900­2100 | CLOSED
```

### Assumptions you can make ###

* NOTAMs will always be separated by one line of white space
* Only NOTAMs containing the exact string ‘AERODROME HOURS OF OPS/SERVICE’ need to be considered
* No parsing of expiry, validity, creation dates or other NOTAM data is required (in essence, only the ‘E’ section need to be parsed)

### Expected Output ###
When you’re done, please send us a link to your rails application on either github/bitbucket. We should be able to clone the application, run it, submit the data in the notam_data.txt file and get the output in the same format as the table above.
Any questions, just send us an e­mail!