require 'rails_helper'

describe Notams::NotamReader do
  let(:reader) { Notams::NotamReader.new raw_data }
  let(:raw_data) { File.read(Rails.root.join('spec/fixtures/notam_data.txt').to_s) }

  it 'saves the raw_data' do
    expect(reader.raw_data).to eq(raw_data)
  end

  it 'returns an array of valid notams' do
    expect(reader.notams.count).to eq(10)
  end
end
