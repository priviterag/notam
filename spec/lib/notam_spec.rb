require 'rails_helper'

describe Notams::Notam do
  let(:notam_data) { <<-EOT }
B0508/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/5746N01404E005
A) ESGJ B) 1503020000 C) 1503222359
E) AERODROME HOURS OF OPS/SERVICE MON  0500-2000 TUE-THU 0500-2100
FRI
0545-2100 SAT0630-0730  1900-2100 SUN CLSD
CREATED: 26 Feb 2015 10:54
SOURCE: EUECYIYN
EOT

  let(:notam) { Notams::Notam.new notam_data }

  it 'returns the raw data' do
    expect(notam.raw_data).to eq(notam_data)
  end

  it 'returns the ICAO' do
    expect(notam.icao).to eq('ESGJ')
  end

  it 'returns an array of days & time' do
    expect(notam.days).to eq(["MON:0500-2000", "TUE:0500-2100", "WED:0500-2100", "THU:0500-2100", "FRI:0545-2100", "SAT:0630-0730,1900-2100", "SUN:CLSD"])
  end

  # this methods are now private
  # I left here just to show how I implemented it using TDD
  #
  # it 'returns the normalised E section' do
  #   expect(notam.esection).to eql('MON:0500-2000 TUE-THU:0500-2100 FRI:0545-2100 SAT:0630-0730,1900-2100 SUN:CLSD')
  # end

  # it 'parse the days' do
  #   expect(notam.normalise_days('MON:0500-2000')).to eq('MON:0500-2000')
  #   expect(notam.normalise_days('MON-WED:0500-2000')).to eq(['MON:0500-2000','TUE:0500-2000','WED:0500-2000'])
  # end

end
