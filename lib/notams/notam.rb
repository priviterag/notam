module Notams

  class Notam
    attr_reader :raw_data

    def initialize raw_data
      @raw_data = raw_data
    end

    def icao
      @icao ||= parse_icao
    end

    def days
      esection.split(' ')
      .map {|s| normalise_days(s) }
      .flatten
    end

    private

    def esection
      @esection ||= normalise_esection(get_esection)
    end

    def normalise_days days
      if days =~ /(MON|TUE|WED|THU|FRI|SAT)-(TUE|WED|THU|FRI|SAT|SUN)/ # interval in the DDD-DDD format
        normalise_interval days # interval
      else
        days # single day
      end
    end

    def normalise_interval days
      starts_ends, times = days.split(':')
      starts, ends = starts_ends.split('-')
      (INDEX_FROM_DAY[starts]..INDEX_FROM_DAY[ends]).map do |i|
        "#{DAY_FROM_INDEX[i]}:#{times}"
      end
    end

    def parse_icao
      raw_data[/A\)\s*(\S*)\s*B\)/m, 1]
    end

    def normalise_esection section
      section
      .gsub(/\s+/,' ') # remove multiple whitespaces
      .gsub(/:/,' ') # remove colon
      .gsub(/,/,' ') # remove comma
      .gsub(/\./,' ') # remove dot
      .gsub(/\n/,' ') # remove newlines
      .gsub(/(MON|TUE|WED|THU|FRI|SAT|SUN)\s?(\d\d\d\d|CLOSED|CLSD)/, '\1:\2') # normalise DDDhhmm-hhmm & DDD hhmm-hhmm
      .gsub(/(\d\d\d\d)\s+(\d\d\d\d)/, '\1,\2') # use comma to separate multiple time intervals in the same day (e.g. SAT 0630-0730  1900-2100)
      .strip
    end

    def get_esection
      raw_data[/E\) AERODROME HOURS OF OPS\/SERVICE\s*(.*)CREATED/m, 1]
    end

    DAY_FROM_INDEX = {
      1 => 'MON',
      2 => 'TUE',
      3 => 'WED',
      4 => 'THU',
      5 => 'FRI',
      6 => 'SAT',
      7 => 'SUN'
    }

    INDEX_FROM_DAY = {
      'MON' => 1,
      'TUE' => 2,
      'WED' => 3,
      'THU' => 4,
      'FRI' => 5,
      'SAT' => 6,
      'SUN' => 7
    }

  end
end
