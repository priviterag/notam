module Notams
  class NotamReader
    attr_reader :raw_data

    def initialize raw_data
      @raw_data = raw_data.gsub("\r\n", "\n")
    end

    def notams
      @notams ||= parse_notams
    end

    private

    def parse_notams
      chunks = raw_data.split(/^\n/) # splits the raw data using empty line as separator
      chunks = chunks.select { |c| c =~ /E\) AERODROME HOURS OF OPS\/SERVICE/ } # filter the notams we want to parse
      chunks.map { |c| Notams::Notam.new c }
    end
  end
end
