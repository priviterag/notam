class NotamsController < ApplicationController
  def edit
  end

  def show
  end

  def decode
    begin
      @notams = Notams::NotamReader.new(params[:notams]).notams
      render :show
    rescue
      redirect_to action: :edit, flash: { error: 'parser error'}
    end
  end
end
